package com.aldrineeinsteen.demo.streams.model;

public enum Category {
    Coupe, Convertible, Wagon, Sedan, Pickup, Hatchback
}
