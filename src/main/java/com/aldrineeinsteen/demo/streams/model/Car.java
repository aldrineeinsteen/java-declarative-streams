package com.aldrineeinsteen.demo.streams.model;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

import java.util.List;

@Setter
@Getter
@AllArgsConstructor
@ToString
public class Car {
    private String make;
    private String model;
    private List<Category> categories;
    private int year;
}
