package com.aldrineeinsteen.demo.streams;

import com.aldrineeinsteen.demo.streams.model.Car;
import com.aldrineeinsteen.demo.streams.model.Category;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

public class App {

    public static void main(String[] args) {
        System.out.println("Hello adventurers.");
        List<Car> cars = setUpCars();

        //Imperative code
        List<Car> convetibles = new ArrayList<>();
        for (Car car: cars) {
            if(car.getCategories().contains(Category.Convertible))
                convetibles.add(car);
        }

        for (Car convertible :
                convetibles) {
            System.out.println(convertible);
        }

        //Declarative code
        System.out.println("With Streams");
        cars.stream().filter(car -> car.getCategories()
                .stream()
                .anyMatch(category -> category.equals(Category.Convertible)))
                .collect(Collectors.toList()).forEach(System.out::println);
    }

    private static List<Car> setUpCars(){
        return List.of(
                new Car("BMW", "1 Series", List.of(Category.Coupe, Category.Convertible), 2013),
                new Car("Audi", "100", List.of(Category.Sedan, Category.Wagon), 1992),
                new Car("Ram", "1500 Crew Cab", List.of(Category.Pickup), 2020),
                new Car("Chevrolet", "1500 Regular Cab", List.of(Category.Pickup), 1998),
                new Car("Polestar", "2", List.of(Category.Hatchback), 2021),
                new Car("Chrysler", "200", List.of(Category.Sedan), 2017),
                new Car("Isuzu", "i-350 Crew Cab", List.of(Category.Pickup), 2006),
                new Car("MINI", "Clubman", List.of(Category.Hatchback), 2011),
                new Car("Lexus", "SC", List.of(Category.Coupe, Category.Convertible), 1992)
        );
    }
}
